import { ModuleWithProviders } from "@angular/core";
export * from './header/header.component';
export default class CliLib {
    static forRoot(): ModuleWithProviders;
}
