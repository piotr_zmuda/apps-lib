import { CliLibPage } from './app.po';

describe('cli-lib App', function() {
  let page: CliLibPage;

  beforeEach(() => {
    page = new CliLibPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
